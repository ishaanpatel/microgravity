##########################################################################################
#                                                   
#           Title:              In Orbit Solar Array Performance fn     
#           Author:             Antonio Coelho                  
#           Date:               03/07/21 
#           Last Maj Update:    03/07/21
#                                                   
##########################################################################################

""" Solar Array Sizing Function"""
import math

def size_sa(saArea,eclipseTime,dayTime,pathEff_eclipse,pathEff_day,saEfficiency,designLife,margin):

    # avePower ...                  [W] average power required during nominal operations
    # eclipseTime ...               [hr] total eclispe time per orbit
    # dayTime ...                   [hr] total day time per orbit
    # pathEff_eclipse ...           [] path efficiency during eclipse operations
    # pathEff_day ...               [] path efficeincy during day operations
    # saEfficiency ...              [] efficiency of solar cell
    # designLife ...                [yr] design life of solar arrays
    # margin ...                    [%] applied margin at solar array box level

    # constants
    solarConstant = float(1368)                                    # [W/m2] solar constant at ~1AU

    # Geometry inputs 
    incidenceAngle = 5                                      # [deg] incidence angle from b/w normal and sun
    incidenceAngle_rad = incidenceAngle*(math.pi/180)         # [rad] 
    
    # other solar cell material properties
    inherentDegradation = 0.77                                      # [] degradation of solar cell material
    yearlyDegradation = 2.75                                        # [%] performance degradation per year of solar cell material
    sa_costFactor = 852                                             # [$/W BOL] approximate cost factor at the beginning of life
    sa_massFactor = 2.7                                             # [kg/m2 BOL] approximate mass factor at the beginning of life

    lifetimeDegradation = (1-(yearlyDegradation/100))**(designLife)     # [%] lifetime degradation of solar cell material

    power_dayOps = ((((avePower*eclipseTime)/pathEff_eclipse)+((avePower*dayTime)/pathEff_day))/dayTime)     # [W] power solar array must provide during daylight
    
    saPO = solarConstant*saEfficiency                                                                                           # [W/m2] power absorption for material

    saPO_BOL = saPO*inherentDegradation*math.cos(incidenceAngle_rad)                                                            # [W/m2] beginning of life power/meter2

    saPO_EOL = saPO_BOL*lifetimeDegradation                                                                                    # [W/m2] end of life power/meter2

    saArea = (power_dayOps/saPO_EOL)*((100+margin)/100)                                                                                             # [m2] solar array surface area required
    saPower_BOL = saArea*saPO_BOL                                                                                               # [W] power production at BOL 
    saPower_EOL = saArea*saPO_EOL                                                                                               # [W] power production at EOL 
    saArea_2 = saPower_BOL/saPO_EOL
    
    saMass = saArea*sa_massFactor                                                                                               # [kg] solar array mass *not including mechanical mounts 
    saCost = saPower_BOL*sa_costFactor                                                                                          # [$] solar array cost
    

    return saPower_BOL,saPower_EOL,saArea,saMass,saCost,saArea_2