##########################################################################################
#                                                   
#           Title:              In Orbit Power System Sizing     
#           Author:             Antonio Coelho                  
#           Date:               01/31/21 
#           Last Maj Update:    01/31/21
#                                                   
##########################################################################################

import matplotlib.pyplot as plt 
import numpy as np 
from BatterySizing import size_battery
from SolarArraySizing import size_sa

### Specify Inputs ###########################################

Period = 1.567                          # [hr] orbital period of 500km circular orbit

""" Power Requirements and Constraints """

req_avePower = 600                    # [W] average power required during nominal operations
req_peakPower = 1200.0                  # [W] peak power required during nominal operations
req_peakPowerTime = 2                   # [hr] time for peak power cycle
req_peakPowerTimeOff = 22                # [hr] time between peak power cycles (battery recharge time)

cst_eclipseTime = 0                     # [hr] time of eclipse per orbit
cst_dayTime = Period - cst_eclipseTime  # [hr] time of day per orbit
req_designLife = 1                   # [yr] design lifetime of space vehicle

""" Battery Performance """

LiIon_energyDensity = 125.0                       # [W-hr/kg] energy density of selected battery
LiIon_energyEfficiency = 98.0                     # [%] energy efficiency of selected battery
LiIon_dod = 45.0                                  # [%] Depth of Discharge 
bat_margin = 10.0                                 # [%] Battery sizing margin

""" Solar Array Performance """

pathEff_eclipse = 0.9                           # [] efficiency of path during eclipse
pathEff_day = 0.9                               # [] efficiency of path during daytime
saEfficiency = 0.185                            # [] efficiency of selected solar cell *Silicon
sa_margin = 10.0                                # [%] solar array sizing margin

### Computations ########################################

""" Solar Array Sizing """
SA_size = size_sa(req_avePower,req_peakPowerTime,req_peakPowerTimeOff,pathEff_eclipse,pathEff_day,saEfficiency,req_designLife,sa_margin)

""" Battery Sizing """
req_battPower = req_peakPower - SA_size[1]    # [W] power that must be provided by the battery (peak power - solar array power EOL)
LiIon_size = size_battery(req_battPower,req_peakPowerTime,LiIon_energyDensity,LiIon_energyEfficiency,LiIon_dod,bat_margin)



### Show Results ########################################
print('Solar Array BOL Power: %1.0f W, Solar Array EOL Power: %1.1f W, Solar Array Area: %1.2f m^2,  Solar Array Mass: %1.2f kg, Solar Array Cost: %1.0f $, Area (computed w/ PowerEOL): %1.2f m^2' % (SA_size))
print('Battery Capacity: %1.1f W-hr, Battery Max Discharged Energy: %1.1f W-hr, Battery Mass: %1.2f kg, Battery Cost: %1.0f $' % (LiIon_size))






