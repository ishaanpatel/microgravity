##########################################################################################
#                                                   
#           Title:              In Orbit Battery Sizing Fn     
#           Author:             Antonio Coelho                  
#           Date:               01/31/21 
#           Last Maj Update:    01/31/21
#                                                   
##########################################################################################

""" Battery Sizing Function"""

def size_battery(batPower,eclipseTime,bat_energyDensity,bat_energyEff,dod,margin):

    # batPower ...                      [W] power required during eclipse
    # eclipseTime ...                   [hr] time of eclipse per orbit
    # bat_energyDensity ...             [W-hr/kg] energy density of selected battery
    # bat_energyEff ...                 [] energy efficiency of selected battery
    # dod ...                           [%] Depth of Discharge (% of total charge discharged per cycle)
    # margin ...                        [%] applied margin at battery box level

    # other material properties
    bat_costFactor = 1.5                # [$/W-hr] cost factor for battery material

    batEnergy = batPower*eclipseTime    
    bat_energyCapacity = batEnergy*((100+margin)/100)/((dod/100))
    bat_dischargedEnergy = bat_energyCapacity*(dod/100)
    bat_mass = (bat_energyCapacity/bat_energyDensity)/(bat_energyEff/100)
    bat_cost = bat_energyCapacity*bat_costFactor

    return bat_energyCapacity, bat_dischargedEnergy, bat_mass, bat_cost